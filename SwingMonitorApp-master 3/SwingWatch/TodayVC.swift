//
//  TodayVC.swift
//  SwingWatch
//
//  Created by Rob Enriquez on 6/13/21.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import UIKit

class TodayVC: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var consultButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func presentCounterVC(_ type: ExerciseType) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CounterVC") as! ViewController
        vc.exerciseType = type
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }

    @IBAction func consultButtonTapped(_ sender: UIButton) {
        presentCounterVC(.golf)
        let image = UIImage(named: "consult")
        imageView.image = image
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        let image = UIImage(named: "today")
        imageView.image = image
    }
}
