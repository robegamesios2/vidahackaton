//
//  ChooseExerciseVC.swift
//  SwingWatch
//
//  Created by Rob Enriquez on 6/10/21.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import UIKit

class ChooseExerciseVC: UIViewController {
    
    @IBOutlet weak var progressButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var p1Button: UIButton!
    @IBOutlet weak var p2Button: UIButton!
    @IBOutlet weak var p3Button: UIButton!
    @IBOutlet weak var p4Button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func progressButtonTapped(_ sender: UIButton) {
        presentProgressVC()
    }
    
    @IBAction func p1ButtonTapped(_ sender: UIButton) {
        presentCounterVC(.golf)
    }
    
    @IBAction func p2BbuttonTapped(_ sender: UIButton) {
        presentCounterVC(.jumpingJack)
    }
    
    @IBAction func p3ButtonTapped(_ sender: UIButton) {
        presentCounterVC(.squat)
    }
    
    @IBAction func p4ButtonTapped(_ sender: UIButton) {
        presentCounterVC(.hulahoop)
    }
    
    func presentCounterVC(_ type: ExerciseType) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CounterVC") as! ViewController
        vc.exerciseType = type
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    func presentProgressVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProgressVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }

}

public enum ExerciseType {
    case golf
    case jumpingJack
    case squat
    case hulahoop
}
