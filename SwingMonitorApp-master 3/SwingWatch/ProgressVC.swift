//
//  ProgressVC.swift
//  SwingWatch
//
//  Created by Srikanth Thota on 6/11/21.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import UIKit

class ProgressVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func progressDismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
