/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 This application's view controller.
 */

import UIKit
import NotificationCenter
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    
    @IBOutlet weak var swingCountLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    let queue = OperationQueue()
    var exerciseType: ExerciseType = .golf
    var imageName = "2"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("View loaded.")
        
        if exerciseType == .golf {
            titleLabel.text = "Concentrate"
            imageName = "3"
            let image = UIImage(named: imageName)
            imageView.image = image
        }
        
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
            print("WatchConnectivity session activated.")
        }
    }
    
    func setCountLabel(_ num: NSNumber){
        let count = num.intValue

        swingCountLabel.text = "\(count)"
        
        
        if count == 1 {
            imageName = "3"
            titleLabel.text = "Great start!"
        } else if count == 2 {
            imageName = "4"
            titleLabel.text = "Keep going"
        } else if count == 3 {
            imageName = "5"
            titleLabel.text = "Awesome Job!!!"
            swingCountLabel.text = "3"
            doneButton.isHidden = false
        } else {
            doneButton.isHidden = true
        }
        let image = UIImage(named: imageName)
        imageView.image = image
    }
    
    func setGolfSuccessSwing() {
        swingCountLabel.font = UIFont.systemFont(ofSize: 100, weight: .bold)
        titleLabel.text = "Perfect Swing!"
        swingCountLabel.text = "100%"
        let image = UIImage(named: "5")
        imageView.image = image
        doneButton.isHidden = false
    }

    @IBAction func doneButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ViewController {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        guard let countInt = message["swingCount"] as? NSNumber else {
            return
        }
        
        print("Got count.")
        
        DispatchQueue.main.async {
            if self.exerciseType == .golf {
                self.setGolfSuccessSwing()
            } else {
                self.setCountLabel(countInt)
            }
        }
    }
}
